### Out of the box features

- Execute Python against the server, giving your character superpowers! [<a href='https://github.com/TeachCraft/RaspberryJuice'>API</a>] [<a href='https://github.com/TeachCraft/TeachCraft-Examples'>Examples</a>]
- Permanent night vision (optional)
- Weather is turned off (optional)
- No hostile mobs
- Permanent nightvision potion effect
- Permanent sword + bow in your inventory (used for interacting with in python) [<a href='https://github.com/TeachCraft/TeachCraft-Examples/blob/master/examples/mcpi_superpowers.py'>Example</a>]
- No authentication required

### To Run

Launch multiplayer server by running this command in your terminal:
```
./start
```

Now when you can connect to this server using Multiplayer -> Direct Connect -> 127.0.0.1

You can change the parameters for how the server launches in <a href='https://github.com/TeachCraft/TeachCraft-Server/blob/master/wrapper.properties.json#L30'>wrapper.properties.json</a>

### To connect to server

- Launch Minecraft v1.11 on Mojang's official launcher [<a href='https://www.youtube.com/watch?v=6honpgnzfcI'>Video</a>]
- Or launch with our <a href='https://gitlab.com/boldidea/curriculum/minecraft-launcher'>custom python launcher</a>

### Options

You can edit `plugins/RaspberryJuice/config.yml` to toggle the following options:

- `enable_night_vision` (true/false)
- `disable_weather` (truy/false)

Note: if you disable night vision on the server, you must kill and re-spawn your character.

### Open Source Libraries Used

- <a href='https://github.com/TeachCraft/RaspberryJuice'>Raspberry Juice</a> (for python api to MC server)
- <a href='https://github.com/TeachCraft/minecraft-wrapper'>minecraft-wrapper</a> (for auto-reboot on crash)
